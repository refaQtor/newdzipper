A tiny sample command-line utility to zip files in [D](http://dlang.org/) with [archive](https://github.com/rcythr/archive) library.
It is my convention to include a serviceable compiling command-line example as a remark at the top of the file with main().
In this case:

```
#!d
dmd archive/*.d newdzipper.d -ofnewdzipper

```