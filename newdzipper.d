//dmd archive/*.d newdzipper.d -ofnewdzipper
module newdzipper;

import std.string, std.stdio, std.file;
import archive.zip;

void main(string[] args)
{
    string fname = args[1];
    auto zip = new ZipArchive();
    auto raw = new ZipArchive.File(fname);
    raw.data = cast(immutable(ubyte)[])read(fname);
    zip.addFile(raw);
    std.file.write(fname ~ ".zip", cast(ubyte[])zip.serialize());
}

